#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QDialog>
#include <QJSEngine>

namespace Ui {
class MainDialog;
}

class MainDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MainDialog(QWidget *parent = nullptr);
    virtual ~MainDialog();

    void test();

protected:
    void runTriggerEvent(QPushButton *pPushbutton);

    void updateData(QString strData);
private slots:
    void on_pushButton_one_clicked();

    void on_pushButton_Two_clicked();

    void on_pushButton_Three_clicked();

    void on_pushButton_Four_clicked();

    void on_pushButton_Five_clicked();

    void on_pushButton_Six_clicked();

    void on_pushButton_Seven_clicked();

    void on_pushButton_Eight_clicked();

    void on_pushButton_Nine_clicked();

    void on_pushButton_Zero_clicked();

    void clickedPushButtonAllClear();

    void clickedPushButtonBackspace();

    void clickedPushButtonPlus();

    void clickedPushButtonMinus();

    void clickedPushButtonMul();

    void clickedPushButtonDiv();

    void clickedpushButtonEqual();

    void initData();

    void updateLCD();

    void checkZero();

    void triggeredActionNumberOne();

    void triggeredActionNumberTwo();

    void triggeredActionNumberThree();

    void triggeredActionNumberFour();

    void triggeredActionNumberFive();

    void triggeredActionNumberSix();

    void triggeredActionNumberSeven();

    void triggeredActionNumberEight();

    void triggeredActionNumberNine();

    void triggeredActionNumberZero();

    void triggeredActionAllClear();

    void triggeredActionBackspace();

    void triggeredActionPlus();

    void triggeredActionMinus();

    void triggeredActionMul();

    void triggeredActionDiv();

    void triggeredActionEqual();


private:
    bool checkOperationValid();

    bool checkLengthValid();

    void initActions();

    void connectActions();

private:
    Ui::MainDialog *ui;

    QString m_strData;      // 11111+11111
    QString m_strDisplay;   // 11,111 + 11,111
    QPushButton *pPrevPushbutton;

    QJSEngine m_Engine; // JAVA Script -> QJSENGINE Class declare -> make a m_Engine object

    bool m_bAfterCalc;


    //@brief m_strDisplay
    //1. 숫자가 3개 이상 연속 될 시 "," 추가.
    //2. 연산자 앞뒤로 빈칸 추가.
    //
    //QString m_operation;
};

#endif // MAINDIALOG_H
