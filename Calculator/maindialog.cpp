﻿#include "maindialog.h"
#include "ui_maindialog.h"

#include <QKeyEvent>
#include <QMessageBox>
#include <QPushButton>
#include <QTimer>

#include <QDebug>

MainDialog::MainDialog(QWidget *parent) : QDialog(parent),
    ui(new Ui::MainDialog),
    m_strData("0"),
    pPrevPushbutton(nullptr),
    m_bAfterCalc(false)
{

    ui->setupUi(this);

    initActions();
    connectActions();

    setWindowTitle("Calculator");

    QObject::connect(ui->pushButton_BackSpace,   SIGNAL(clicked()),  this, SLOT(clickedPushButtonBackspace()));
    QObject::connect(ui->pushButton_AllClear,    SIGNAL(clicked()),  this, SLOT(clickedPushButtonAllClear()));
    QObject::connect(ui->pushButton_Plus,        SIGNAL(clicked()),  this, SLOT(clickedPushButtonPlus()));
    QObject::connect(ui->pushButton_Mul,         SIGNAL(clicked()),  this, SLOT(clickedPushButtonMul()));
    QObject::connect(ui->pushButton_Div,         SIGNAL(clicked()),  this, SLOT(clickedPushButtonDiv()));
    QObject::connect(ui->pushButton_Minus,       SIGNAL(clicked()),  this, SLOT(clickedPushButtonMinus()));
    QObject::connect(ui->pushButton_Equal,       SIGNAL(clicked()),  this, SLOT(clickedpushButtonEqual()));

    updateLCD();
}

MainDialog::~MainDialog()
{
    delete ui;
}

void MainDialog::updateData(QString strData)
{
    checkZero();

    if(m_bAfterCalc)
    {
        m_strData = strData;
        m_bAfterCalc = false;
    }
    else
    {
        m_strData.append(strData);
    }

    updateLCD();
}

void MainDialog::on_pushButton_one_clicked()
{
    updateData("1");
}

void MainDialog::on_pushButton_Two_clicked()
{
    updateData("2");
}

void MainDialog::on_pushButton_Three_clicked()
{
    updateData("3");
}

void MainDialog::on_pushButton_Four_clicked()
{
    updateData("4");
}

void MainDialog::on_pushButton_Five_clicked()
{
    updateData("5");
}


void MainDialog::on_pushButton_Six_clicked()
{
    updateData("6");
}

void MainDialog::on_pushButton_Seven_clicked()
{
    updateData("7");
}

void MainDialog::on_pushButton_Eight_clicked()
{
    updateData("8");
}

void MainDialog::on_pushButton_Nine_clicked()
{
    updateData("9");
}

void MainDialog::on_pushButton_Zero_clicked()
{
    if(m_strData.endsWith(" "))
    {
        return;
    }

    if(m_strData=="0") // -> After
    {
        m_strData = "0";
        m_bAfterCalc = false;
    }
    else
    {
        m_strData.append("0");
        // updataLCD() 함수를 호출하기전 사칙기호 뒤에 0이있는지 없는지 체크
        updateLCD();
    }
}

void MainDialog::clickedPushButtonAllClear()
{
    m_bAfterCalc = false;

    m_strData.clear();
    m_strData = "0";

    updateLCD();
}

void MainDialog::clickedPushButtonBackspace()
{
    m_bAfterCalc = false;

    int removeIndex = m_strData.size()-1; // 현재 스트링 사이즈에 -1을 한 후 removeIndex에 저장
    m_strData.remove(removeIndex, 1); //removeIndex 변수에 들어가있는 수만큼 원소제거


    if(removeIndex==0) //removeIndex에 저장된 사이즈 수가 0이면 LCD에는 빈 공간이 아닌 다시 0으로 초기화
    {
        m_strData = "0";
    }

    updateLCD();
}

void MainDialog::clickedPushButtonPlus()
{
    m_bAfterCalc = false;

    bool checkValid_return= checkOperationValid();
    if(false==checkValid_return)
    {

        return;
    }

    else
    {

        m_strData.append(" + "); //연산자 앞뒤로 빈칸 추가
        updateLCD();
    }

}

void MainDialog::clickedPushButtonMinus()
{
    m_bAfterCalc = false;

    bool checkValid_return= checkOperationValid();
    if(false==checkValid_return)
    {

        return;
    }

    else
    {

        m_strData.append(" - "); //연산자 앞뒤로 빈칸 추가
        updateLCD();
    }
}

void MainDialog::clickedPushButtonMul()
{
    m_bAfterCalc = false;

    bool checkValid_return= checkOperationValid();
    if(false==checkValid_return)
    {

        return;
    }

    else
    {

        m_strData.append(" * "); //연산자 앞뒤로 빈칸 추가
        updateLCD();
    }
}

void MainDialog::clickedPushButtonDiv()
{
    m_bAfterCalc = false;

    bool checkValid_return = checkOperationValid();
    if(false == checkValid_return ) //스트링에 0또는 사칙 연산자 기호가 있으면 거짓을 return 시켜서 이벤트 발생시키지 않는다.
    {

        return;
    }

    else // false가 아닐경우에는 각 사칙연산을 현재 스트링에 append 시킨다.
    {

        m_strData.append(" / "); //연산자 앞뒤로 빈칸 추가
        updateLCD();
    }



}

void MainDialog::clickedpushButtonEqual()
{
    QJSValue jsValue = m_Engine.evaluate(m_strData);
    QString strResult = jsValue.toString();
    m_strData = strResult;
    updateLCD();

    bool isError = jsValue.isError();
    if(isError || m_strData=="Infinity")  // 계산 오류나 Infinity 출력이 되는 경우 0.7초 동안 에러 메세지를 출력하고 initData 함수를 호출
    {                                     // 후 m_strData 스트링에 0으로 초기화
        QTimer::singleShot(700, [=](){
            this->initData();
        });
    }

    m_bAfterCalc = true;

}

void MainDialog::initData()
{
    m_strData = "0";
    this->updateLCD();
}

void MainDialog::updateLCD()
{
    //123 + 1234 + 48 + 7792 + 123


    //[0] create Index Array o
    QList<int> tIndexList;

    //[1] for m_strData, reverse o
    int nCnt=0;
    int nMaxSize = m_strData.size();
    int nStringSize = m_strData.size();

    //qDebug() << nStringSize;

    //123456789
    for(int i = nStringSize-1; i > 0; --i)
    {
        //[2] in for, get QChar Object
        QChar charater = m_strData.at(i);
        bool bIsNumber = charater.isNumber();

        //[3] in for, check number -> true, false
        if(true == bIsNumber)
        {
            //[4] in for, if true, save index to Array
            nCnt++;
            if((nCnt % 3) == 0)
            {
                if(i != 0)
                {
                    QChar charater = m_strData.at(i - 1);
                    if(charater.isNumber())
                    {
                        tIndexList.append(i);
                        qDebug() << "Index : " << i;

                    }
                }
            }
        }

        else
        {
            nCnt=0;
        }
    }

    //[5] create local QString object - QString object = "";
    QString strDisplay = m_strData;
    //[6] for Array
    foreach (int nIndex , tIndexList) {
        //[7] in for, QString insert index ","
        strDisplay.insert(nIndex, ",");
    }

    //[8] save local QString value to m_strDisplay
    m_strDisplay = strDisplay;

    //[9] set LineEdit text m_strDisplay
    QLineEdit *pLineEdit =  ui->Lcd;
    checkLengthValid();
    pLineEdit->setText(m_strDisplay);
}

void MainDialog::checkZero()
{
    if(m_strData != "0") //처음 0인지 아닌지를 판단 후 0이 아니면 스트링을 빈문자열로 초기화 시키지 않고 리턴
        return;

    m_strData = ""; // 만약 zero 상태라면  빈문자열로 초기화
}

void MainDialog::triggeredActionNumberOne()
{
    runTriggerEvent(ui->pushButton_one);
}

void MainDialog::triggeredActionNumberTwo()
{
    runTriggerEvent(ui->pushButton_Two);
}

void MainDialog::triggeredActionNumberThree()
{
    runTriggerEvent(ui->pushButton_Three);
}

void MainDialog::triggeredActionNumberFour()
{
    runTriggerEvent(ui->pushButton_Four);
}

void MainDialog::triggeredActionNumberFive()
{
    runTriggerEvent(ui->pushButton_Five);
}

void MainDialog::triggeredActionNumberSix()
{
    runTriggerEvent(ui->pushButton_Six);
}

void MainDialog::triggeredActionNumberSeven()
{
    runTriggerEvent(ui->pushButton_Seven);
}

void MainDialog::triggeredActionNumberEight()
{
    runTriggerEvent(ui->pushButton_Eight);
}

void MainDialog::triggeredActionNumberNine()
{
    runTriggerEvent(ui->pushButton_Nine);
}

void MainDialog::triggeredActionNumberZero()
{
    runTriggerEvent(ui->pushButton_Zero);
}

void MainDialog::triggeredActionAllClear()
{
    runTriggerEvent(ui->pushButton_AllClear);
}

void MainDialog::triggeredActionBackspace()
{
    runTriggerEvent(ui->pushButton_BackSpace);
}

void MainDialog::triggeredActionPlus()
{
    runTriggerEvent(ui->pushButton_Plus);
}

void MainDialog::triggeredActionMinus()
{
    runTriggerEvent(ui->pushButton_Minus);
}

void MainDialog::triggeredActionMul()
{
    runTriggerEvent(ui->pushButton_Mul);
}

void MainDialog::triggeredActionDiv()
{
    runTriggerEvent(ui->pushButton_Div);
}

void MainDialog::triggeredActionEqual()
{
    runTriggerEvent(ui->pushButton_Equal);
}

void MainDialog::runTriggerEvent(QPushButton *pPushbutton)
{
    if(pPrevPushbutton)
    {
        pPrevPushbutton->setAttribute(Qt::WA_UnderMouse, false);
    }

    pPushbutton->setAttribute(Qt::WA_UnderMouse, true);
    repaint();

    emit pPushbutton->clicked();

    pPrevPushbutton = pPushbutton;

    QTimer::singleShot(300, [=](){
        pPushbutton->setAttribute(Qt::WA_UnderMouse, false);
        this->repaint();
    });
}


bool MainDialog::checkOperationValid()
{
    if(m_strData=="0")
    {
        return false;
    }

    if(m_strData.endsWith(" "))
    {
        return false;
    }

    return true;
}

bool MainDialog::checkLengthValid()
{
    int nMax = 22;
    if(m_strData.size() > nMax)
    {
        QMessageBox messagebox;
        messagebox.setWindowTitle("Error!");
        messagebox.setText("Please Input below 10-digit");
        messagebox.exec();
    }

    /**
     * @brief message
     *
     */


    return true;
}

void MainDialog::initActions()
{
    this->addAction(ui->actionNumberOne);
    this->addAction(ui->actionNumberTwo);
    this->addAction(ui->actionNumberThree);
    this->addAction(ui->actionNumberFour);
    this->addAction(ui->actionNumberFive);
    this->addAction(ui->actionNumberSix);
    this->addAction(ui->actionNumberSeven);
    this->addAction(ui->actionNumberEight);
    this->addAction(ui->actionNumberNine);
    this->addAction(ui->actionNumberZero);
    this->addAction(ui->actionPlus);
    this->addAction(ui->actionMinus);
    this->addAction(ui->actionMul);
    this->addAction(ui->actionDiv);
    this->addAction(ui->actionEqual);
    this->addAction(ui->actionBackSpace);
    this->addAction(ui->actionAllClear);


}
void MainDialog::connectActions()
{
    connect(ui->actionNumberOne,    SIGNAL(triggered()), this,    SLOT(triggeredActionNumberOne()));
    connect(ui->actionNumberTwo,    SIGNAL(triggered()), this,    SLOT(triggeredActionNumberTwo()));
    connect(ui->actionNumberThree,  SIGNAL(triggered()), this,    SLOT(triggeredActionNumberThree()));
    connect(ui->actionNumberFour,   SIGNAL(triggered()), this,    SLOT(triggeredActionNumberFour()));
    connect(ui->actionNumberFive,   SIGNAL(triggered()), this,    SLOT(triggeredActionNumberFive()));
    connect(ui->actionNumberSix,    SIGNAL(triggered()), this,    SLOT(triggeredActionNumberSix()));
    connect(ui->actionNumberSeven,  SIGNAL(triggered()), this,    SLOT(triggeredActionNumberSeven()));
    connect(ui->actionNumberEight,  SIGNAL(triggered()), this,    SLOT(triggeredActionNumberEight()));
    connect(ui->actionNumberNine,   SIGNAL(triggered()), this,    SLOT(triggeredActionNumberNine()));
    connect(ui->actionNumberZero,   SIGNAL(triggered()), this,    SLOT(triggeredActionNumberZero()));
    connect(ui->actionPlus,         SIGNAL(triggered()), this,    SLOT(triggeredActionPlus()));
    connect(ui->actionMinus,        SIGNAL(triggered()), this,    SLOT(triggeredActionMinus()));
    connect(ui->actionMul,          SIGNAL(triggered()), this,    SLOT(triggeredActionMul()));
    connect(ui->actionDiv,          SIGNAL(triggered()), this,    SLOT(triggeredActionDiv()));
    connect(ui->actionEqual,        SIGNAL(triggered()), this,    SLOT(triggeredActionEqual()));
    connect(ui->actionBackSpace,    SIGNAL(triggered()), this,    SLOT(triggeredActionBackspace()));
    connect(ui->actionAllClear,     SIGNAL(triggered()), this,    SLOT(triggeredActionAllClear()));
}

