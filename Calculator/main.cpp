#include "maindialog.h"
#include <QApplication>
#include <QFile>


void initStyleSheet()
{
    QFile File(":/folder/stylesheet.qss");
    File.open(QFile::ReadOnly);
    QString strStyleSheet = QLatin1String(File.readAll());

    qApp->setStyleSheet(strStyleSheet);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    initStyleSheet();

    MainDialog w;

    w.show();


    return a.exec();
}
